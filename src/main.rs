#![warn(clippy::pedantic)]

use bracket_lib::prelude::*;
// use rand::Rng;
use tracing::debug;
// use tracing::info;
use tracing_subscriber;

enum GameMode {
    Menu,
    Playing,
    End,
}

const SCREEN_WIDTH: i32 = 40;
const SCREEN_HEIGHT: i32 = 25;
const MARGIN: i32 = 5;
const MIN_GAP_SIZE: i32 = 4;
const MAX_GAP_SIZE: i32 = 10;
const FRAME_DURATION: f32 = 75.0;
const DRAGON_FRAMES: [u16; 6] = [64, 1, 2, 3, 2, 1];

struct Player {
    x: i32,
    y: f32,
    velocity: f32,
    frame: usize,
}

impl Player {
    fn new(x: i32, y: i32) -> Self {
        Player {
            x,
            y: y as f32,
            velocity: 0.0,
            frame: 0,
        }
    }

    fn gravity_and_move(&mut self) {
        if self.velocity < 2.0 {
            self.velocity += 0.1;
        }

        //    Apply gravit
        self.y += self.velocity;
        if self.y < 0.0 {
            self.y = 0.0;
        }

        //    Move the player
        self.x += 1;
        self.frame += 1;
        self.frame = self.frame % DRAGON_FRAMES.len();
        debug!(self.frame, "frame");
    }

    fn flap(&mut self) {
        self.velocity = -1.0;
    }

    fn render(&mut self, context: &mut BTerm) {
        // context.set(0, self.y, YELLOW, BLACK, to_cp437('@'));
        context.set_active_console(1);
        context.cls();
        context.set_fancy(
            PointF::new(0.0, self.y),
            1,
            Degrees::new(0.0),
            PointF::new(2.0, 2.0),
            WHITE,
            NAVY,
            DRAGON_FRAMES[self.frame],
        );
        context.set_active_console(0);
    }
}

struct Obstacle {
    x: i32,

    ///    Where in the obstacle the gap for the player is
    gap_y: i32,

    ///    Length of the gab in the obstacle
    size: i32,
}

impl Obstacle {
    fn new(x: i32, score: i32) -> Self {
        let mut random = RandomNumberGenerator::new();

        Obstacle {
            x,
            gap_y: random.range(MARGIN, SCREEN_HEIGHT - MARGIN),

            //    As the user gets a higher and higher score,
            //    reduce the size of the gap the dragon can
            //    fit through.  Leave a minimum size of
            //    two spaces.
            size: i32::max(MIN_GAP_SIZE, MAX_GAP_SIZE - score),
        }
    }

    fn render(&mut self, context: &mut BTerm, player_x: i32) {
        // The ground
        draw_ground(context);

        let screen_x = self.x - player_x;
        let half_size = self.size / 2;

        // Draw the top half of the obstacle
        self.draw_half_wall(context, screen_x, 0, self.gap_y - half_size);
        self.draw_half_wall(context, screen_x, self.gap_y + half_size, SCREEN_HEIGHT - 1);
    }

    fn draw_half_wall(&mut self, context: &mut BTerm, screen_x: i32, start: i32, end: i32) {
        for y in start..end {
            // context.set(screen_x, y, RED, BLACK, to_cp437('/'));
            context.set(screen_x, y, WHITE, NAVY, 186);
        }
    }

    fn hit_obstacle(&mut self, player: &Player) -> bool {
        let half_size = self.size / 2;
        let does_x_match = player.x == self.x;
        let player_above_gap = (player.y as i32) < self.gap_y - half_size;
        let player_below_gap = (player.y as i32) > self.gap_y + half_size;
        does_x_match && (player_above_gap || player_below_gap)
    }
}

fn draw_ground(context: &mut BTerm) {
    for x in 0..SCREEN_WIDTH {
        context.set(x, SCREEN_HEIGHT - 1, WHITE, WHITE, to_cp437('#'));
    }
}

struct State {
    player: Player,
    frame_time: f32,
    obstacle: Obstacle,
    mode: GameMode,
    score: i32,
}

impl State {
    fn new() -> Self {
        State {
            //    Start the player at the bottom
            player: Player::new(MARGIN, SCREEN_HEIGHT),
            frame_time: 0.0,
            obstacle: Obstacle::new(SCREEN_WIDTH, 0),
            mode: GameMode::Menu,
            score: 0,
        }
    }

    fn restart(&mut self) {
        //    Start the player in the center vertically?
        self.player = Player::new(5, SCREEN_HEIGHT / 2);
        self.frame_time = 0.0;
        //    Start the obstacle at the right edge, let
        //    it move left.
        self.obstacle = Obstacle::new(SCREEN_WIDTH, 0);
        self.mode = GameMode::Playing;
        self.score = 0;
    }

    fn main_menu(&mut self, context: &mut BTerm) {
        self.print_menu(context);
        self.handle_key_input(context);
    }

    fn handle_key_input(&mut self, context: &mut BTerm) {
        if let Some(key) = context.key {
            match key {
                VirtualKeyCode::P => self.restart(),
                VirtualKeyCode::Q => context.quitting = true,
                _ => {}
            }
        }
    }

    fn dead(&mut self, context: &mut BTerm) {
        self.print_menu(context);
        self.handle_key_input(context);
    }

    fn play(&mut self, context: &mut BTerm) {
        context.cls_bg(NAVY);
        self.frame_time += context.frame_time_ms;

        self.throttle_play();

        if let Some(VirtualKeyCode::Space) = context.key {
            self.player.flap();
        }

        self.player.render(context);
        context.print(0, 0, "Press SPACE to flap");
        context.print(0, 1, &format!("Score: {}", self.score));

        self.obstacle.render(context, self.player.x);
        if self.player_still_alive() {
            self.score += 1;
            //    New wall, starting at the right.
            self.obstacle = Obstacle::new(self.player.x + SCREEN_WIDTH, self.score);
        }

        if self.player_dead() {
            self.mode = GameMode::End;
        }
    }

    fn player_dead(&mut self) -> bool {
        self.player_hit_bottom() || self.player_hit_obstacle()
    }

    fn player_hit_obstacle(&mut self) -> bool {
        self.obstacle.hit_obstacle(&self.player)
    }

    fn player_hit_bottom(&mut self) -> bool {
        self.player.y as i32 > SCREEN_HEIGHT
    }

    fn player_still_alive(&mut self) -> bool {
        self.player.x > self.obstacle.x
    }

    fn throttle_play(&mut self) {
        if self.frame_time > FRAME_DURATION {
            self.frame_time = 0.0;

            self.player.gravity_and_move();
        }
    }

    fn print_menu(&self, context: &mut BTerm) {
        context.cls();
        match self.mode {
            GameMode::Menu => {
                context.print_color_centered(5, YELLOW, BLACK, "Welcome to Flappy Dragon")
            }
            GameMode::End => context.print_color_centered(5, RED, BLACK, "You are dead!"),
            _ => {}
        };

        if self.score > 0 {
            context.print_centered(6, &format!("You earned {} points", self.score));
        }

        context.print_color_centered(8, CYAN, BLACK, "(P) Play Again");
        context.print_color_centered(9, CYAN, BLACK, "(Q) Quit Game");
    }
}

impl GameState for State {
    fn tick(&mut self, context: &mut BTerm) {
        match self.mode {
            GameMode::Menu => self.main_menu(context),
            GameMode::End => self.dead(context),
            GameMode::Playing => self.play(context),
        }
        // context.print(1, 1, "Hello, Bracket Terminal");
    }
}

// Options: Kjammer_16x16, Md_16x16, Yayo16x16, Zilk16x16, Md_16x16, flappy32
// bracket_terminal::embedded_resource!(TILE_FONT, "../resources/Md_16x16.png");
// bracket_terminal::embedded_resource!(TILE_FONT, "../resources/flappy32.png");

fn main() -> BError {
    standard_program_init();
    // bracket_terminal::link_resource!(TILE_FONT, "resources/Md_16x16.png");
    // let mut rng = rand::thread_rng();
    let context = BTermBuilder::new()
        .with_font("../resources/flappy32.png", 32, 32)
        .with_simple_console(SCREEN_WIDTH, SCREEN_HEIGHT, "../resources/flappy32.png")
        .with_fancy_console(SCREEN_WIDTH, SCREEN_HEIGHT, "../resources/flappy32.png")
        // .with_font("Md_16x16.png", 16, 16)
        .with_title("Flappy Dragon Enhanced")
        .with_tile_dimensions(16, 16)
        // .with_automatic_console_resize(true)
        .build()?;

    main_loop(context, State::new())
}

///
///    Initializes standard things at program startup.
///
///    These include:
///    * color_eyre
///    * tracing
///
fn standard_program_init() {
    color_eyre::install().unwrap();

    // install global collector configured based on RUST_LOG env var.
    tracing_subscriber::fmt::init();
}
